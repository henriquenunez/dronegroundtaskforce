#!/bin/bash

cd ~/Workspace/multi_robot_ws

source /opt/ros/galactic/setup.bash
# source /opt/ros/galactic/setup.zsh
# source /usr/share/gazebo/setup.sh

colcon build

source install/setup.bash
# source install/setup.zsh

export GAZEBO_MODEL_PATH=~/.gazebo/models/gazebo_models/

ros2 launch dgtf_main launch.py
