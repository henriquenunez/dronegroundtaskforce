"""Simulate a Tello drone"""

import os

from ament_index_python.packages import get_package_share_directory
from launch import LaunchDescription
from launch_ros.actions import Node
from launch.actions import ExecuteProcess

from launch.actions import IncludeLaunchDescription
from launch.launch_description_sources import PythonLaunchDescriptionSource
import launch_ros.actions
import os
import launch.actions

from launch import LaunchDescription
from launch.actions import IncludeLaunchDescription
from launch.launch_description_sources import PythonLaunchDescriptionSource
from launch.substitutions import PathJoinSubstitution
from launch_ros.substitutions import FindPackageShare


def generate_launch_description():
    ns = 'drone1'

    world_path = os.path.join(get_package_share_directory('dgtf_main'), 'worlds', 'main.world')
    urdf_path = os.path.join(get_package_share_directory('tello_description'), 'urdf', 'tello_1.urdf')

    # Tello
    package_name = "tello_gazebo"  
    package_address = get_package_share_directory(package_name)
    rl_params_file = os.path.join("dgtf_main", "config", "dual_ekf_navsat_params.yaml")
    rviz_file = os.path.join("dgtf_main", "rviz", "view.rviz")

    print("Address of package '{}' is: {}".format(package_name, package_address))

    # Husky
    world_file = PathJoinSubstitution(
        [FindPackageShare("husky_gazebo"),
        "worlds",
        "clearpath_playpen.world"],
    )

    gazebo_launch = PathJoinSubstitution(
        [FindPackageShare("husky_gazebo"),
        "launch",
        "gazebo_launch.py"],
    )

    gazebo_sim = IncludeLaunchDescription(
        PythonLaunchDescriptionSource([gazebo_launch]),
        launch_arguments={'world_path': world_file}.items(),
    )

    # ld.add_action(gazebo_sim)

    return LaunchDescription([
        # Launch Gazebo, loading tello.world
        # ExecuteProcess(cmd=[
        #     'gazebo',
        #     '--verbose',
        #     '-s', 'libgazebo_ros_init.so',  # Publish /clock
        #     '-s', 'libgazebo_ros_factory.so',  # Provide gazebo_ros::Node
        #     world_path
        # ], output='screen'),
        gazebo_sim,

        # Spawn tello.urdf
        Node(package='tello_gazebo', executable='inject_entity.py', output='screen', arguments=[urdf_path, '0', '0', '3', '1.57079632679']),

        # Publish static transforms
        Node(package='robot_state_publisher', executable='robot_state_publisher', output='screen', arguments=[urdf_path]),
        
        Node(package='dgtf_main', executable='dgtf_main', output='screen'),
        Node(package='rviz2', executable='rviz2', namespace=ns, output='screen', arguments=[rviz_file]),

    ])
