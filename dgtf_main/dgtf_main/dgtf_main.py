import rclpy
from rclpy.node import Node
from sensor_msgs.msg import Image, NavSatFix, NavSatStatus
import cv2
from cv_bridge import CvBridge
from std_msgs.msg import Float32,Float32MultiArray
from sensor_msgs.msg import Image
from rclpy.action import ActionClient
from tello_msgs.srv import TelloAction
from geometry_msgs.msg import Twist
import random
import time
import math

class Timer:
    def __init__(self, max_ticks):
        self.max_ticks = max_ticks
        self.ready = False

    def reset(self):
        self.curr_ticks = self.max_ticks

    def tick(self):
        if self.curr_ticks > 0:
            self.curr_ticks -= 1
            self.ready = False
        else:
            self.ready = True

class TelloController(Node):

    def __init__(self):
        super().__init__('tello_image_reciever')
        self.get_logger().info("Controller node initialized")
        self.go_through = 0

        self.base_img_subscription = self.create_subscription(
            Image,
            '/drone1_base_cam/image_raw',
            self.image_cb,
            rclpy.qos.qos_profile_sensor_data
        )

        self.gps_subscription = self.create_subscription(\
            NavSatFix,
            '/gps/fix',
            self.gps_cb,
            rclpy.qos.qos_profile_sensor_data
        )
        
        # self.fps_sub = self.create_subscription(
        #     Float32,
        #     '/fps',
        #     self.fps_cb,
        #     rclpy.qos.qos_profile_sensor_data
        # )

        # self.fps = 15

        self.tello_vel_pub = self.create_publisher(Twist, '/drone1/cmd_vel', 10)
        self.tello_client = self.create_client(TelloAction, '/drone1/tello_action')
        self.img_test_pub = self.create_publisher(Image, '/drone1/test_img', 10)
        self.husky_vel_pub = self.create_publisher(Twist, '/husky_velocity_controller/cmd_vel_unstamped', 10)

        # time.sleep(3)
        
        while not self.tello_client.wait_for_service(timeout_sec=2.0):
            self.get_logger().info('Waiting for TelloAction service')

        self.tello_req = TelloAction.Request()
        self.tello_req.cmd = 'takeoff'

        takeoff_future = self.tello_client.call_async(self.tello_req)
        rclpy.spin_until_future_complete(self, takeoff_future)

        self.get_logger().info('took off!')

    def gps_cb(self, data):
        # self.get_logger().info(f'GPS data. Latitude: {data.latitude} Longitude: {data.longitude} Altitude: {data.altitude}')
        pass

    def image_cb(self, data):
        # self.get_logger().info(f'Frame data: {data.data}')
        # self.get_logger().info(f'Got Image!')

        current_frame = CvBridge().imgmsg_to_cv2(data)
        img_inverted = cv2.bitwise_not(current_frame)
        pub_frame = CvBridge().cv2_to_imgmsg(img_inverted)
        self.img_test_pub.publish(pub_frame)

    def control(self):

        twist_msg = Twist()

        twist_msg.linear.x = 0.1
        twist_msg.linear.y = 0.0
        twist_msg.linear.z = 0.0
        twist_msg.angular.x = 0.0
        twist_msg.angular.y = 0.0
        twist_msg.angular.z = 0.1

        return twist_msg

    def send_vel_cmd(self):
        twist_msg = self.control()
                
        if twist_msg is not None:
            self.tello_vel_pub.publish(twist_msg)

    def control_husky(self):
        twist_msg = Twist()

        twist_msg.linear.x = 0.5
        twist_msg.linear.y = 0.0
        twist_msg.linear.z = 0.0
        twist_msg.angular.x = 0.0
        twist_msg.angular.y = 0.0
        twist_msg.angular.z = 0.0

        # self.husky_vel_pub.publish(self.control())
        self.husky_vel_pub.publish(twist_msg)

def main(args=None):
    rclpy.init(args=args)

    tello_controll = TelloController()

    # rclpy.spin(tello_controll)
    # Manual
    while rclpy.ok():
        rclpy.spin_once(tello_controll, timeout_sec=0.5)
        tello_controll.send_vel_cmd()
        tello_controll.control_husky()

    tello_controll.destroy_node()
    rclpy.shutdown()

if __name__ == '__main__':
    main()

# def main():
#     print('Hi from dgtf_main.')


# if __name__ == '__main__':
#     main()
